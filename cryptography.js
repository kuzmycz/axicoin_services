const crypto = require("crypto");
const algorithm = "aes-256-cbc";

function decrypt(secret, seed, cipherText) {
    const decipher = crypto.createDecipheriv(algorithm, secret, seed);

    let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
    decryptedData += decipher.final("utf8");

    return decryptedData;
}

function encrypt(secret, seed, clearText) {

    const cipher = crypto.createCipheriv(algorithm, secret, seed);
    var encryptedData = cipher.update(clearText, "utf-8", "hex");
    encryptedData += cipher.final("hex");

    return encryptedData;
}

exports.encryptText = async (accountId, plainText) => {
    if (!encryptedText) throw 'plainText is required';

    if (!accountId) {
        // decrypt text with master key
        return decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, encryptedText)
    } else {
        let data = await ddb.get({
            TableName: "account_keys",
            Key: {
                accountId: accountId
            }
        }).promise();

        if (data && data.key && data.seed) {
            var secret = decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, data.key);
            var seed = decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, data.seed);

            var plainText = decrypt(secret, seed, encryptedText);

            return plainText;
        }

        throw "Unable to get encryption keys";
    }

}

exports.decryptText = async (accountId, cipherText) => {

    if (!cipherText) throw 'cipherText is required';

    if (!accountId) {
        // decrypt text with master key
        return decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, encryptedText)
    } else {
        let data = await ddb.get({
            TableName: "account_keys",
            Key: {
                accountId: accountId
            }
        }).promise();

        if (data && data.key && seed) {
            var secret = decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, data.key);
            var seed = decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, data.seed);

            var plainText = decrypt(secret, seed, encryptedText);

            return plainText;
        }
        
        throw "Unable to get decryption keys";
    }
};