const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const web3 = require("web3");
const cryptography = require("cryptography");

const getAccountId = async (email) => {
    try {
        let data = await ddb.get({
            TableName: "user_account",
            Key: {
                email: "email"
            }
        }).promise();

        return data.accountId;

    } catch (err) {
        // error handling goes here
        console.log('Unable to find the user\'s account');
    };
}

const getWalletAddress = async (accountId) => {
    try {
        let data = await ddb.get({
            TableName: "account_keys",
            Key: {
                accountId: accountId
            }
        }).promise();

        const privateKey = cryptography.decryptText(accountId, data.privateKey);
        const account = web3.eth.accounts.privateKeyToAccount(privateKey);


        return account.address;

    } catch (err) {
        // error handling goes here
    };
}

exports.handler = async (event) => {
    const { email } = event;

    const address = getWalletAddress(getAccountId(email));

    return address;
};