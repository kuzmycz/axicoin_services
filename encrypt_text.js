const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const crypto = require("crypto");
const algorithm = "aes-256-cbc";

function decrypt(secret, seed, cipherText) {
    const decipher = crypto.createDecipheriv(algorithm, process.env.MASTER_KEY, process.env.MASTER_SEED);

    let decryptedData = decipher.update(encryptedData, "hex", "utf-8");
    decryptedData += decipher.final("utf8");

    return decryptedData;
}

function encrypt(secret, seed, clearText) {

    const cipher = crypto.createCipheriv(algorithm, secret, seed);
    var encryptedData = cipher.update(clearText, "utf-8", "hex");
    encryptedData += cipher.final("hex");

    return encryptedData;
}

exports.handler = async (event) => {

    var { accountId, clearText } = event;
    if (!clearText) throw 'clearText is required';

    if (!accountId) {
        // encrypt text with master key
        return encrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, clearText);
    } else {

        try {
            let data = await ddb.get({
                TableName: "account_keys",
                Key: {
                    accountId: accountId,
                }
            }).promise();

            if (data && data.key) {
                var secret = decrypt(process.env.MASTER_KEY, process.env.MASTER_SEED, data.key);
                var seed = decrype(process.env.MASTER_KEY, process.env.MASTER_SEED, data.seed);

                var encryptedData = encrypt(secret, seed, clearText);

                return encryptedData;
            }

        } catch (err) {
            // error handling goes here
        };
    }
};