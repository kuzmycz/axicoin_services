const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const Web3 = require("web3");
const { v4: uuidv4 } = require('uuid');

exports.handler = async (event) => {
    const web3 = new Web3(process.env.BLOCKCHAIN_ADDRESS);
    const { accountId } = event;

    const account = web3.eth.accounts.create();
    console.log("Here", event);
    const userAccount = {
        id: uuidv4(),
        accountId: accountId,
        address: account.address,
        privateKey: account.privateKey,
    };

    try {
        let data = await ddb.put({
            TableName: "coin_address",
            Item: userAccount
        }).promise();

    } catch (err) {
        // error handling goes here
        console.error(`Unable to persist the account details for ${accountId}`, account.address);
    };

    return { account: accountId, address: account.address };
};