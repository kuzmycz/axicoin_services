const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const { v4: uuidv4 } = require('uuid');
const crypto = require("crypto");
const coder = require("./cryptography");

exports.handler = async (event) => {
    const { email, firstName, lastName, phoneNumber } = event;
    console.log('env:', event);
    console.log('env:', email);
    const userAccount = {
        id: uuidv4(),
        email: email,
        firstName: firstName,
        lastName: lastName,
        phoneNumber: phoneNumber,
    };
    try {
        let data = await ddb.put({
            TableName: "user_account",
            Item: userAccount
        }).promise();

        let result2 = await ddb.put({
            TableName: "account_keys",
            Item: {
                accountId: userAccount.id,
                key: coder.encryptText(undefined, crypto.randomBytes(32)),
                seed: coder.encryptText(undefined, crypto.randomBytes(16)),
            }
        }).promise();

    } catch (err) {
        // error handling goes here
        console.error(`Unable to persist the account details for ${email}`, data, result2);
    };

    return userAccount;
};